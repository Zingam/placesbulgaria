/** 
* @projectDescription    Places Bulgaria
*
* @author   Roccoor Multimedia
* @version  1.0
*/

// Create namespace ///////////////////////////////////////////////////////////
if (!window.com) com = {};
if (!com.roccoor) com.roccoor = {};
if (!com.roccoor.bmi) com.roccoor.mainmenu = {};

// Functions //////////////////////////////////////////////////////////////////

/**
 * Opens a screen with BBM connectivity
 * @alias register
 * @alias com.roccoor.mainmenu.register
 */
com.roccoor.mainmenu.display_screenShareOnBbm = function() {
	bb.pushScreen('screenShareOnBbm.html', 'screenShareOnBbm');
}

/**
 * Opens a screen with information about Bulgaria and disclaimer
 * @alias display_screenAbout
 * @alias com.roccoor.mainmenu.display_screenAbout
 */
com.roccoor.mainmenu.display_screenAbout = function() {
	bb.pushScreen('screenAbout.html', 'screenAbout');
}

display_screenBachkovo = function() {
	bb.pushScreen('content/Bachkovo/screenBachkovo.html', 'screenBachkovo');
}

display_screenBeklemeto = function() {
	bb.pushScreen('content/Beklemeto/screenBeklemeto.html', 'screenBeklemeto');
}

display_screenKazanlakTomb = function() {
	bb.pushScreen('content/KazanlakTomb/screenKazanlakTomb.html', 'screenKazanlakTomb');
}

display_screenKlisura = function() {
	bb.pushScreen('content/Klisura/screenKlisura.html', 'screenKlisura');
}

display_screenMalyovitsa = function() {
	bb.pushScreen('content/Malyovitsa/screenMalyovitsa.html', 'screenMalyovitsa');
}

display_screenOstrushaTomb = function() {
	bb.pushScreen('content/OstrushaTomb/screenOstrushaTomb.html', 'screenOstrushaTomb');
}

display_screenPamporovo = function() {
	bb.pushScreen('content/Pamporovo/screenPamporovo.html', 'screenPamporovo');
}

display_screenPlovdiv = function() {
	bb.pushScreen('content/Plovdiv/screenPlovdiv.html', 'screenPlovdiv');
}

display_screenSevenRilaLakes = function() {
	bb.pushScreen('content/SevenRilaLakes/screenSevenRilaLakes.html', 'screenSevenRilaLakes');
}

display_screenShipka = function() {
	bb.pushScreen('content/Shipka/screenShipka.html', 'screenShipka');
}

display_screenSofia = function() {
	bb.pushScreen('content/Sofia/screenSofia.html', 'screenSofia');
}

display_screenVelikoTarnovo = function() {
	bb.pushScreen('content/VelikoTarnovo/screenVelikoTarnovo.html', 'screenVelikoTarnovo');
}

display_screenVihren = function() {
	bb.pushScreen('content/Vihren/screenVihren.html', 'screenVihren');
}